<?php
error_reporting(!(E_USER_NOTICE & E_WARNING & E_DEPRECATED));
// reading upload file
$file = $_FILES["xrayfile"];
$newname = $_POST["newname"];

if (!$file) {
    echo "It seems you did not attach a file or whatever";
    exit;
}

$newname = ( $newname == "" ? $file["name"] : $newname ) . ".dat";
$content = file_get_contents($file["tmp_name"]);

$numbers_array = preg_split("/\n/", $content); // php < 5.4 sucks!
$numbers = $numbers_array[3];
$data = preg_split("/\s+/", $numbers);
$row = 11;
$first = true;

header("Content-type: text/plain");
header("Content-Disposition: attachment; filename=$newname");
foreach ($data as $datum) {
    if ($first) {
        $first = false;
        continue;
    }
    // writing line to file to download
    echo number_format($row, 1) . "\t" . $datum . "\r\n";
    
    $row+=0.2;
}
