<!DOCTYPE html>
<!--
[CC Zero] Base < base-w @ yandex.ru >
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Convert weird x-ray format to .dat</title>
    </head>
    <body>
        <form enctype="multipart/form-data" action="toDat.php" method="POST">
            <p>Put the file to be converted here: <input name="xrayfile" type="file" />
            <p>Specify the output file name if different from original: <input name="newname" type="text" placeholder="H-O" />.dat
            <p><input type="submit" value="Convert file" />
        </form>
    </body>
</html>
